class UserMailer < ApplicationMailer
	default :from => "apps.panha@gmail.com"
	def new_contact_email(contact)
		@contact = contact
		mail(:to=>contact.email, :subject=>"New contact")
	end
end
