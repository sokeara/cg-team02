class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :firstName
      t.string :lastName
      t.string :phone
      t.string :email
      t.text :message

      t.timestamps null: false
    end
  end
end
